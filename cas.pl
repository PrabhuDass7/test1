use strict;
use WWW::Mechanize;
use URI::Escape;
use IO::Socket::SSL qw();

my $mech = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);


$mech->add_header( 'Accept' => 'application/json, text/plain, */*');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
$mech->add_header( 'Authorization' => 'Bearer gWlR39JG8SgjDwSmwmgVdUBvNzXuUGChLyQnpL0YSS_znxQTMnB73H3aCcKs_NLqKecrydIkL_MvDU-JJbXJLlIddBJub-H5UqddonkMl4BqWFRa1rmjYX_Qqz76Nsy9aiaYY405OBGrYyOVTb3QUw6UImXXrURcGO49SNMaWi6t_bPg-_5GAroPWBt-Mg0_Kg_X15YFxeShrZFo5samgw');
$mech->add_header( 'Connection' => 'keep-alive');
$mech->add_header( 'Content-Type' => 'application/json;charset=UTF-8');
$mech->add_header( 'Cookie' => '_ga=GA1.2.1483147248.1606542475; _gid=GA1.2.617742988.1606542475');
$mech->add_header( 'Host' => 'rec.pottcounty-ia.gov');
$mech->add_header( 'Origin' => 'https://rec.pottcounty-ia.gov');
$mech->add_header( 'Referer' => 'https://rec.pottcounty-ia.gov/IAPottawattamie/AvaWeb/');
$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36');

my $post_cont='{"lastBusinessName":"","firstName":"","startDate":"2020-11-20T08:00:00.000Z","endDate":"2020-11-27T08:00:00.000Z","documentName":"","documentType":"","documentTypeName":"","book":"","page":"","subdivisionName":"","subdivisionLot":"","subdivisionBlock":"","municipalityName":"","tractSection":"","tractTownship":"","tractRange":"","tractQuarter":"","tractQuarterQuarter":"","addressHouseNo":"","addressStreet":"","addressCity":"","addressZip":"","parcelNumber":"","referenceNumber":""}';
# my $post_cont='{"id":6560331}';

print "post_cont:: $post_cont\n";

my $posturl="https://rec.pottcounty-ia.gov/IAPottawattamie/Scrap.WebService.Ava/breeze/breeze/Search";
$mech->post( $posturl, Content => "$post_cont");
my $pingStatus = $mech->status;		

print "Home Page ping Status==>$pingStatus\n";
my $con=$mech->content();
print "con:::$con\n\n";
open F1, ">POST_CONTENT.html";
print F1 $con;
close F1;
